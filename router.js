import express from "express";
import { renderHtml } from "./controller/mainController.js";
import { checkHealth } from "./controller/checkHealthController.js";
import { biodata } from "./controller/biodataController.js";
import { checkAuthKey } from "./middleware/mainMiddleware.js";
// import {
//   get as getProduct,
//   create as createProduct,
//   update as updateProduct,
//   destroy as deleteProduct,
//   find as findProduct,
// } from "./controller/productController.js";
import productApi from "./api/productApi.js";
import bookApi from "./api/bookApi.js";
import officeApi from "./api/officeApi.js";

const router = express.Router();

router.get("/", renderHtml);
router.get("/check-health", checkHealth);
router.post("/biodata", checkAuthKey, biodata);
router.use("/products", productApi);
router.use("/books", bookApi);
router.use("/offices", officeApi);

// router.get("/product/list", getProduct);
// router.post("/product/create", createProduct);
// router.put("/product/update", updateProduct);
// router.delete("/product/destroy", deleteProduct);
// router.get("/product/find", findProduct);

export default router;
