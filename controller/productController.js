export const get = (req, res) => {
  return res.render("products/index", {
    name: `${req.body.nama}`,
    alamat: ` ${req.body.alamat}`,
    productA: ` ${req.body.productA}`,
  });
};
export const create = (req, res) => {
  return res.send("create product");
};
export const update = (req, res) => {
  return res.send("update product");
};
export const destroy = (req, res) => {
  return res.send("destroy product");
};
export const find = (req, res) => {
  return res.render("products/find", {
    productA: ` ${req.body.productA}`,
  });
};
