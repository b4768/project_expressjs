export const renderHtml = (req, res) => {
  return res.render("index", {
    name: req.query.name || "Guest",
  });
};
