export const checkAuthKey = (req, res, next) => {
  if (req.headers["auth-key"] === "#4311") {
    next();
    return;
  }
  return res.status(401).send("Kamu belum memasukkan auth-key dengan benar");
};
