const offices = [];

export default {
  create(params) {
    const office = {
      name: params.name,
      address: params.address,
    };

    offices.push(office);

    return office;
  },
  list() {
    return offices;
  },
  find(id) {
    const book = offices.find((i) => i.id === Number(id));
    if (!book) return null;

    return book;
  },
};
